#include "Sound.h"

Sound::Sound(const std::string_view filePath) : 
m_chunk(nullptr),
m_soundFilePath(filePath)
{
	assert(typeid(filePath) == typeid(std::string_view) && !filePath.empty() && "Sound filepath cannot have an empty value");
}

Sound::~Sound()
{
	if (m_chunk != nullptr)
	{
		Mix_FreeChunk(m_chunk);
		printf("SOUND UNLOADED: \t---> \t%s\n", m_soundFilePath.c_str());
	}
}

Mix_Chunk* Sound::Load()
{
	m_chunk = Mix_LoadWAV(m_soundFilePath.c_str());
	if (m_chunk == nullptr)
	{
		printf("Error loading audio file. Error: %s\n", Mix_GetError());
	}

	return(m_chunk);
}