#pragma once

#include <SDL_mixer.h>
#include <cassert>
#include <cstdio>
#include <string>

class Sound
{
public:
	Sound()=default;
	Sound(const std::string_view filePath);
	~Sound();

	Mix_Chunk* Load();
	inline void Play() const { Mix_PlayChannel(-1, m_chunk, 0); }
	inline void setVolume(int v) const { Mix_VolumeChunk(m_chunk, MIX_MAX_VOLUME / v); }

private:
	//Sound(const Sound& obj)=delete;
	//Sound& operator=(const Sound&) = delete;

	Mix_Chunk* m_chunk;
	std::string m_soundFilePath;
};