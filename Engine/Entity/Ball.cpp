#include "Ball.h"

Ball::Ball(unsigned int x, unsigned int y, unsigned int speed) :
m_posX((x / 2) - 40), m_posY((y/ 2) - 25), m_angle(0),
m_ballTexture("Assets/Graphics/ball.png"), m_ballRect({0}),
m_ballWidth(50), m_ballHeight(50),
m_velocityX(0.0), m_velocityY(0.0),
m_speed(0), m_isDead(false)
{
	assert(typeid(x) == typeid(unsigned int) && x > 0 && "Ball must have an X value");
	assert(typeid(y) == typeid(unsigned int) && y > 0 && "Ball must have an Y value");

	switch (speed)
	{
		case 0:
		{
			m_speed = 35;
		} break;
		case 1:
		{
			m_speed = 50;
		} break;
		case 2:
		{
			m_speed = 70;
		} break;

	default:
		break;
	}

	setAngle(randomizeAng());
}

Ball::Ball(unsigned int w, unsigned int h, unsigned int x, unsigned int y) :
m_posX(x), m_posY(y), m_angle(0),
m_ballRect({ 0, 0, 0, 0 }),
m_ballWidth(w), m_ballHeight(h),
m_velocityX(0.0), m_velocityY(0.0),
m_speed(0), m_isDead(false)
{
	assert(w > 0);
	assert(h > 0);
	assert(x > 0);
	assert(y > 0);

	m_ballTexture = Texture("Assets/Graphics/ball.png");
	setAngle(randomizeAng());
}

void Ball::resetBall(unsigned int w, unsigned int h, unsigned int difficulty)
{
	m_posX = (w / 2) - 40;
	m_posY = (h / 2) - 25;

	setAngle(randomizeAng());
	m_isDead = false;

	switch (difficulty)
	{
		case 0:
		{
			m_speed = 35;
		} break;
		case 1:
		{
			m_speed = 50;
		} break;
		case 2:
		{
			m_speed = 70;
		} break;

	default:
		break;
	}
}

void Ball::Load(SDL_Renderer* passedRenderer)
{
	m_ballTexture.Load(passedRenderer);
}

void Ball::Draw(SDL_Renderer* passedRenderer)
{
	m_ballRect.w = m_ballWidth;
	m_ballRect.h = m_ballHeight;
	m_ballRect.x = m_posX;
	m_ballRect.y = m_posY;

	if (m_isDead)
	{
		SDL_SetTextureColorMod(m_ballTexture.getTexture(), 255, 0, 0);
	}
	else if (!m_isDead)
	{
		SDL_SetTextureColorMod(m_ballTexture.getTexture(), 255, 255, 255);
	}

	SDL_RenderCopy(passedRenderer, m_ballTexture.getTexture(), nullptr, &m_ballRect);
}

void Ball::Update(double delta)
{
	m_posX += static_cast<int>(m_velocityX * delta);
	m_posY += static_cast<int>(m_velocityY * delta);

	//printf("Speed: %i\n", m_speed);
}

void Ball::setAngle(double angle)
{
	m_angle = angle;

	m_velocityX = ((cos(m_angle)) * m_speed);
	m_velocityY = ((sin(m_angle)) * m_speed);
}

double Ball::randomizeAng()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> dis(-3.0f, 3.0f);

	double r = dis(gen);

	return(r);
}