#include "Texture.h"

Texture::Texture(const std::string_view filePath) :
m_filePath(filePath),
m_textureWidth(0), m_textureHeight(0),
m_texture(nullptr),
m_textureRect({0})
{
	assert(typeid(filePath) == typeid(std::string_view) && !filePath.empty() && "Texture filepath cannot be empty");
}

Texture::~Texture()
{
	if (m_texture != nullptr)
	{
		SDL_DestroyTexture(m_texture);
		printf("TEXTURE DESTROYED: \t---> \t%s\n", m_filePath.c_str());
	}
}

SDL_Texture* Texture::Load(SDL_Renderer* passedRen)
{
	m_texture = IMG_LoadTexture(passedRen, m_filePath.c_str());
	if (m_texture == nullptr)
	{
		printf("TEXTURE: \t---> Error loading texture file. Error: \t%s\n", IMG_GetError());
	}
	else
	{
		if (SDL_QueryTexture(m_texture, nullptr, nullptr, &m_textureWidth, &m_textureHeight) == -1)
		{
			printf("TEXTURE: \t---> \t%s is invalid\n", SDL_GetError());
		}
		else
		{
			printf("TEXTURE LOADED: \t---> \t%s\n", m_filePath.c_str());
		}
	}

	return(m_texture);
}

void Texture::Unload()
{
	m_texture = nullptr;
	SDL_DestroyTexture(m_texture);

	printf("TEXTURE UNLOADED: \t---> \t%s\n", m_filePath.c_str());
}

void Texture::setRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	m_textureRect.x = x;
	m_textureRect.y = y;
	m_textureRect.w = w;
	m_textureRect.h = h;
}

void Texture::scale(unsigned int scaler)
{
	m_textureRect.w = (m_textureRect.w * scaler);
	m_textureRect.h = (m_textureRect.h * scaler);
}