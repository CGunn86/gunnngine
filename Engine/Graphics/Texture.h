#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <cassert>
#include <cstdio>
#include <string>

class Texture
{
public:
	Texture()=default;
	Texture(const std::string_view filePath);
	~Texture();

	SDL_Texture* Load(SDL_Renderer* passedRen);
	void Unload();
	void setRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h);
	void scale(unsigned int scaler);
	inline unsigned int getWidth() const { return m_textureWidth; }
	inline unsigned int getHeight() const { return m_textureHeight; }
	inline std::string getFilePath() const { return m_filePath; }

	inline SDL_Texture* getTexture() const { return m_texture; }
	inline SDL_Rect getRect() const { return m_textureRect; }

private:
	//Texture(const Texture& obj)=delete;
	//Texture& operator=(const Texture&)=delete;

	std::string m_filePath;
	int m_textureWidth;
	int m_textureHeight;

	SDL_Texture* m_texture;
	SDL_Rect m_textureRect;
};