#include "Gunngine.h"

Gunngine::Gunngine(const std::string_view t, unsigned int w, unsigned int h) :
m_window(nullptr), m_renderer(nullptr), m_title(t), m_screenWidth(w), m_screenHeight(h),
m_difficulty(1), m_health(550), m_bonusProgress(0), m_lives(3),
m_convertedLives(nullptr),
m_keyBoard(false), m_mouse(true), m_mouseX(0), m_mouseY(0),
m_paddles({nullptr}), m_items({}), m_itemNum(0),
m_deltaTime(0.0), m_currentTime(uint64_t(0ULL)), m_lastTime(uint64_t(0ULL)),
m_running(false), m_paused(true), m_gameOver(false), m_levelWon(false), m_newGame(true), m_cached(false)
{
	assert(typeid(t) == typeid(std::string_view) && !t.empty() && "Window must have a title");
	assert(typeid(w) == typeid(unsigned int) && w > 0 && "Window width must be greater than 0");
	assert(typeid(h) == typeid(unsigned int) && h > 0 && "Window height must be greater than 0");
}

Gunngine::~Gunngine()
{
	cleanUp();
}

void Gunngine::updateDelta()
{
	m_lastTime = m_currentTime;
	m_currentTime = SDL_GetPerformanceCounter();
	m_deltaTime = static_cast<double>((m_currentTime - m_lastTime) * 10 / static_cast<double>(SDL_GetPerformanceFrequency()));
}

void Gunngine::setupGameObjects()
{
	printf("<-----LOADING GAME--------->\n");

	//Define Drops
	m_items[0]              = std::make_unique<Item>(Type::DamagePaddles);
	m_items[1]              = std::make_unique<Item>(Type::SwapPaddles);
	m_items[2]              = std::make_unique<Item>(Type::IncreaseSpeed);
	m_items[3]              = std::make_unique<Item>(Type::DecreaseSpeed);
	m_items[4]              = std::make_unique<Item>(Type::Health);

	//Define Level Object
	m_dungeonLevels         = LevelSet("Assets/Graphics/Levels/Dungeon/", m_screenWidth, m_screenHeight);
	setCache(false);

	//Define paddles
	m_paddles[0]            = std::make_unique<Paddle>((m_screenWidth / 6.5), 15, (m_screenWidth / 2) - (175 / 2), 30);
	m_paddles[1]            = std::make_unique<Paddle>(15, (m_screenHeight / 4), m_screenWidth - 45, (m_screenHeight / 2) - (175 / 2));
	m_paddles[2]            = std::make_unique<Paddle>((m_screenWidth / 6.5), 15, (m_screenWidth / 2) - (175 / 2), m_screenHeight - 45);
	m_paddles[3]            = std::make_unique<Paddle>(15, (m_screenHeight / 4), 30, (m_screenHeight / 2) - (175 / 2));

	//Define ball
	m_ball                  = std::make_unique<Ball>(m_screenWidth, m_screenHeight, m_difficulty);

	//TODO: Rewrite the text class entirely, this is shit (Calvin)
	//Define text components
	m_gameTitle				= std::make_unique<Text>(m_renderer, 48, "KEEP IT ALIVE");
	m_gameTitleStart        = std::make_unique<Text>(m_renderer, 12, "Press SPACE to start or ESC to exit");
	m_pausedText            = std::make_unique<Text>(m_renderer, 48, "PAUSED");
	m_pressSpaceText        = std::make_unique<Text>(m_renderer, 12, "Press SPACE to continue");
	m_gameOverText          = std::make_unique<Text>(m_renderer, 48, "GAME OVER");
	m_gameOverNewGameText   = std::make_unique<Text>(m_renderer, 12, "Press N to start a new game or ESC to exit");
	m_levelPassed           = std::make_unique<Text>(m_renderer, 48, "LEVEL COMPLETE");
	m_levelPassedContinue   = std::make_unique<Text>(m_renderer, 12, "Press N to continue to the next level");

	//m_gameTitle           = Text(m_renderer, 48, "KEEP IT ALIVE");
	//m_gameTitleStart      = Text(m_renderer, 12, "Press SPACE to start or ESC to exit");
	//m_pausedText          = Text(m_renderer, 48, "PAUSED");
	//m_pressSpaceText      = Text(m_renderer, 12, "Press SPACE to continue");
	//m_gameOverText        = Text(m_renderer, 48, "GAME OVER");
	//m_gameOverNewGameText = Text(m_renderer, 12, "Press N to start a new game or ESC to exit");
	//m_levelPassed         = Text(m_renderer, 48, "LEVEL COMPLETE");
	//m_levelPassedContinue = Text(m_renderer, 12, "Press N to continue to the next level");

	//Define misc. graphical components
	m_dead                  = Texture("Assets/Graphics/gameover.png");
	m_cursor                = Texture("Assets/Graphics/UI/cursor.png");

	//Define audio components
	m_hitSound				= Sound("Assets/Sounds/hitnormal.wav");
	m_hitBadSound			= Sound("Assets/Sounds/hitbad.wav");
	m_pauseSound			= Sound("Assets/Sounds/pause.wav");
	m_gameOverSound			= Sound("Assets/Sounds/gameover.wav");
	m_heal					= Sound("Assets/Sounds/heal.wav");
	m_bgMusic				= Music("Assets/Sounds/bgmusic.wav");

	//Load HUD
	m_HUD.Load(m_renderer, m_screenWidth, m_health, true);

	//Load Items
	for (size_t i = 0; i < m_items.size(); ++i)
	{
		m_items[i]->Load(m_renderer);
	}

	//Load Level Sets
	if (m_cached)
	{
		m_dungeonLevels.LoadAll(m_renderer);
	}
	else
	{
		m_dungeonLevels.Load(m_renderer);
	}

	m_dungeonLevels.setScores({ 10, 50, 175, 250, 300 });

	//Load all audio
	m_hitSound.Load();
	m_hitBadSound.Load();
	m_pauseSound.Load();
	m_gameOverSound.Load();
	m_heal.Load();
	m_heal.setVolume(4);
	m_bgMusic.Load();

	//Load all textures
	m_ball->Load(m_renderer);
	m_dead.Load(m_renderer);
	m_dead.setRect((m_screenWidth / 2) - (m_dead.getWidth() / 2), m_screenHeight - m_dead.getHeight(), m_dead.getWidth(), m_dead.getHeight());
	m_cursor.Load(m_renderer);
}

bool Gunngine::init()
{
	//Initialize SDL and it's sub systems
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) != 0)
	{
		printf("Error starting SDL. Error: %s\n", SDL_GetError());
		return(false);
	}

	if ((IMG_Init(IMG_INIT_PNG != IMG_INIT_PNG)))
	{
		printf("IMG_Init: Failed to init required PNG support!\n");
		printf("IMG_Init: %s\n", IMG_GetError());
		return(false);
	}

	if (TTF_Init() != 0)
	{
		printf("Error starting SDL_TTF. Error: %s\n", TTF_GetError());
		return(false);
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0)
	{
		printf("Error starting SDL_Mixer. Error: %s\n", Mix_GetError());
		return(false);
	}

	m_window = SDL_CreateWindow(
		m_title.data(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		m_screenWidth,
		m_screenHeight,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (m_window == nullptr)
	{
		printf("Error creating game window. Error: %s\n", SDL_GetError());

		SDL_DestroyWindow(m_window);
		m_window = nullptr;

		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
		SDL_Quit();

		return(false);
	}

	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_renderer == nullptr)
	{
		printf("Error creating SDL_Renderer. Error: %s\n", SDL_GetError());

		SDL_DestroyWindow(m_window);
		m_window = nullptr;

		SDL_DestroyRenderer(m_renderer);
		m_renderer = nullptr;

		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
		SDL_Quit();

		return(false);
	}

	setupGameObjects();

	GameRunning(true);
	m_bgMusic.Play();

	return(true);
}

void Gunngine::processInput()
{
	SDL_Event evnt;
	while (SDL_PollEvent(&evnt))
	{
		switch (evnt.type)
		{
			case SDL_QUIT:
			{
				GameRunning(false);
			} break;

			case SDL_USEREVENT: {
				const auto timer = reinterpret_cast<timer_t*>(evnt.user.data1);
				if (timer != nullptr)
					timer->callback(&m_timerPool, timer);
			} break;

			//TODO: Consider a key input manager if needed, might not be (Calvin)
			case SDL_KEYDOWN:
			{
				switch (evnt.key.keysym.sym)
				{
					case SDLK_ESCAPE:
					{
						GameRunning(false);
					} break;

					case SDLK_SPACE:
					{
						if (!m_paused)
						{
							Mix_PauseMusic();
							m_paused = true;
							m_pauseSound.Play();
						}
						else if (m_levelWon)
						{
							m_paused = true;
						}
						else if (m_paused && !m_gameOver)
						{
							Mix_ResumeMusic();
							m_paused = false;
							m_pauseSound.Play();
						}

						if (!m_keyBoard.isEnabled() && !m_mouse.isEnabled())
						{
							printf("No input devices enabled. Enable either the mouse or the keyboard!\n");
							m_paused = true;
						}
						else if (m_newGame)
						{
							m_newGame = false;
						}
					} break;

					case SDLK_n:
					{
						if (m_gameOver || m_levelWon)
						{
							resetGame();
							m_bgMusic.Play();
							m_gameOver = false;
							m_levelWon = false;
							m_newGame = true;
						}
					} break;

					case SDLK_m:
					{
						if (m_bgMusic.isMuted())
						{
							m_bgMusic.Unmute();
							printf("Unmuted\n");

						}
						else if (!m_bgMusic.isMuted())
						{
							m_bgMusic.Mute();
							printf("Muted\n");
						}
					} break;

					case SDLK_h:
					{
						if (m_HUD.isShowing())
						{
							m_HUD.setShowing(false);
						}
						else
						{
							m_HUD.setShowing(true);
						}
					} break;

					default:
						break;
				}
			} break;

			default:
				break;
		}
	}
}

void Gunngine::drawLevel() const
{
	m_dungeonLevels.Draw(m_renderer);
}

void Gunngine::drawPaddles() const
{
	for (size_t i = 0; i < m_paddles.size(); ++i)
	{
		m_paddles[i]->Draw(m_renderer);
	}
}

void Gunngine::drawBall() const
{
	m_ball->Draw(m_renderer);
}

//TODO: Really need a game state manager to avoid this kind of code
void Gunngine::drawText() const
{
	if (m_HUD.isShowing())
	{
		m_HUD.Draw(m_renderer, m_screenWidth);
	}

	if (m_gameOver)
	{
		SDL_Rect rect = m_dead.getRect();
		SDL_RenderCopy(m_renderer, m_dead.getTexture(), nullptr, &rect);
		m_gameOverText->Draw(m_renderer, (m_screenWidth / 2) - 216, (m_screenHeight / 2) - 150);
		m_gameOverNewGameText->Draw(m_renderer, (m_screenWidth / 2) - 225, (m_screenHeight / 2) - 90);
	}

	else if (m_levelWon)
	{
		m_levelPassed->Draw(m_renderer, (m_screenWidth / 2) - 336, (m_screenHeight / 2) - 150);
		m_levelPassedContinue->Draw(m_renderer, (m_screenWidth / 2) - 222, (m_screenHeight / 2) - 90);
	}

	else if (m_paused && !m_newGame)
	{
		m_pausedText->Draw(m_renderer, (m_screenWidth / 2) - 144, (m_screenHeight / 2) - 150);
		m_pressSpaceText->Draw(m_renderer, (m_screenWidth / 2) - 130, (m_screenHeight / 2) - 90);
	}

	else if (m_paused && m_newGame)
	{
		m_gameTitle->Draw(m_renderer, (m_screenWidth / 2) - 255, (m_screenHeight / 2) - 150);
		m_gameTitleStart->Draw(m_renderer, (m_screenWidth / 2) - 185, (m_screenHeight / 2) - 90);
	}
}

void Gunngine::drawCursor() const
{
	SDL_Rect r = m_cursor.getRect();
	SDL_RenderCopy(m_renderer, m_cursor.getTexture(), NULL, &r);
}

void Gunngine::drawItems() const
{
	m_items[m_itemNum]->Draw(m_renderer);
}

void Gunngine::Draw() const
{
	beginRender();

	drawLevel();
	drawText();

	if (m_gameOver || m_levelWon)
	{
		if (m_HUD.m_ScoreBoard->getScore() >= 100)
		{
			m_HUD.m_ScoreBoard->showFinal(m_renderer, (m_screenWidth / 2) - 235, (m_screenHeight / 2) - 70);
		}
		else if (m_HUD.m_ScoreBoard->getScore() < 10)
		{
			m_HUD.m_ScoreBoard->showFinal(m_renderer, (m_screenWidth / 2) - 190, (m_screenHeight / 2) - 70);
		}
		else if (m_HUD.m_ScoreBoard->getScore() < 99)
		{
			m_HUD.m_ScoreBoard->showFinal(m_renderer, (m_screenWidth / 2) - 225, (m_screenHeight / 2) - 70);
		}
	}

	drawBall();

	if (!m_paused)
	{
		drawPaddles();
	}
	else
	{
		drawCursor();
	}

	if (m_bonusProgress >= 550)
	{
		drawItems();
	}

	endRender();
}

Paddle* Gunngine::getPaddle()
{
	if (m_collider.CheckCollision(m_paddles[0]->getRect(), m_ball->getRect()))
	{
		return(m_paddles[0].get());
	}

	if (m_collider.CheckCollision(m_paddles[1]->getRect(), m_ball->getRect()))
	{
		return(m_paddles[1].get());
	}

	if (m_collider.CheckCollision(m_paddles[2]->getRect(), m_ball->getRect()))
	{
		return(m_paddles[2].get());
	}

	if (m_collider.CheckCollision(m_paddles[3]->getRect(), m_ball->getRect()))
	{
		return(m_paddles[3].get());
	}

	return(nullptr);
}

void Gunngine::checkCollision()
{
	Paddle* paddleHit = getPaddle();

	if (paddleHit == nullptr)
	{
		return;
	}

	if (paddleHit->isMarked())
		m_hitBadSound.Play();
	else
	{
		m_hitSound.Play();
		if (m_bonusProgress <= 540)
		{
			m_bonusProgress += 10;
		}
		else
		{
			m_bonusProgress = 0;
			spawnItem();
		}
	}

	bool fTopBottom = paddleHit == m_paddles[0].get() || paddleHit == m_paddles[2].get(); /*||
		              paddleHit == m_levelPaddles[0].get() || paddleHit == m_levelPaddles[2].get();*/

	double dimension = fTopBottom ? paddleHit->getRect().w : paddleHit->getRect().h;

	double dist = fTopBottom ? (m_ball->getRect().x + m_ball->getRect().w / 2) - paddleHit->getRect().x :
		(m_ball->getRect().y + m_ball->getRect().h / 2) - paddleHit->getRect().y;

	double percent = static_cast<double>(dist / dimension);

	if (percent > 1)
		percent = 1;
	if (percent < 0)
		percent = 0;

	double angle = 0.0f;

	if (paddleHit == m_paddles[0].get())
	{
		angle = percent * -1 * M_PI + M_PI;
		m_ball->m_posY = paddleHit->getRect().y + paddleHit->getRect().h + 1;
	}
	else if (paddleHit == m_paddles[1].get())
	{
		angle = percent * -1 * M_PI - M_PI / 2;
		m_ball->m_posX = paddleHit->getRect().x - m_ball->getRect().w - 1;
	}
	else if (paddleHit == m_paddles[2].get())
	{
		angle = percent * M_PI - M_PI;
		m_ball->m_posY = paddleHit->getRect().y - m_ball->getRect().h - 1;
	}
	else if (paddleHit == m_paddles[3].get())
	{
		angle = percent * M_PI - M_PI / 2;
		m_ball->m_posX = paddleHit->getRect().x + paddleHit->getRect().w + 1;
	}

	m_ball->setAngle(angle);

	if (!fTopBottom)
	{
		if (paddleHit->getRect().h >= m_ball->getRect().w && paddleHit->isMarked())
		{
			//Default was "5"
			paddleHit->setRectH(paddleHit->getRect().h - (paddleHit->getRect().h / 16));
			m_health -= 75;
			m_ball->addSpeed(2);
		}
		else if (paddleHit->getRect().h >= m_ball->getRect().w)
		{
			//Default was "2"
			paddleHit->setRectH(paddleHit->getRect().h - (paddleHit->getRect().h / 20));
			m_health -= 10;
			m_ball->addSpeed(1);
		}
	}
	else
	{
		if (paddleHit->getRect().w >= m_ball->getRect().w && paddleHit->isMarked())
		{
			paddleHit->setRectW(paddleHit->getRect().w - (paddleHit->getRect().w / 16));
			m_health -= 75;
			m_ball->addSpeed(2);
		}
		else if (paddleHit->getRect().w >= m_ball->getRect().w)
		{
			paddleHit->setRectW(paddleHit->getRect().w - (paddleHit->getRect().w / 20));
			m_health -= 10;
			m_ball->addSpeed(1);
		}
	}

	if (m_HUD.m_ScoreBoard->getScore() > 0 && paddleHit->isMarked())
	{
		m_HUD.m_ScoreBoard->decreaseScore(1);
	}
	else if (!paddleHit->isMarked())
	{
		m_HUD.m_ScoreBoard->increaseScore(1);
	}

	paddleHit->setHit(true);
}

void Gunngine::checkforBonus()
{
	if (m_paddles[0]->isMarked() && m_paddles[1]->isMarked() && m_paddles[2]->isMarked() && m_paddles[3]->isMarked())
	{
		if (m_health <= 484)
		{
			m_health += 66;
		}
		else
		{
			m_health = 550;
		}

		if (m_bonusProgress <= 500)
		{
			m_bonusProgress += 50;
		}
		else
		{
			m_bonusProgress = 0;
			spawnItem();
		}

		m_heal.Play();
		m_HUD.m_ScoreBoard->increaseScore(5);
		m_ball->removeSpeed(3);
		
		for (size_t i = 0; i < m_paddles.size(); ++i)
		{
			m_paddles[i]->Heal();
		}
	}
}

void Gunngine::checkforGameOver()
{
	if (m_health <= 0 || m_ball->getRect().x <= 0 || m_ball->getRect().x > m_screenWidth - m_ball->getRect().w || m_ball->getRect().y < 0 || m_ball->getRect().y > m_screenHeight - m_ball->getRect().h)
	{
		m_bgMusic.Stop();
		m_health = 0;
		m_bonusProgress = 0;
		m_gameOverSound.Play();

		m_ball->setDead(true);
		m_paused = true;
		if (m_HUD.m_ScoreBoard->getScore() > m_HUD.m_ScoreBoard->getHighScore())
		{
			m_HUD.m_ScoreBoard->recordHighScore();
		}

		if (m_HUD.m_ScoreBoard->getScore() >= m_HUD.m_ScoreBoard->getLevelScore())
		{
			m_levelWon = true;
		}
		else
		{
			--m_lives;
			m_gameOver = true;
		}
	}
}

bool Gunngine::checkforWin()
{
	if (m_HUD.m_ScoreBoard->getScore() >= m_HUD.m_ScoreBoard->getLevelScore())
	{
		m_dungeonLevels.nextLevel(m_renderer, m_cached);

		return(true);
	}

	return(false);
}

void Gunngine::spawnItem()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> dis(0, 5);

	m_itemNum = dis(gen);

	m_items[m_itemNum]->setPos(100, 24);
}

void Gunngine::Update()
{
	updateDelta();

	SDL_GetMouseState(&m_mouseX, &m_mouseY);

	m_cursor.setRect(m_mouseX, m_mouseY, 24, 44);

	if (m_health <= 0)
	{
		m_health = 0;
	}

	m_HUD.Update(m_renderer, m_dungeonLevels, *m_ball, m_lives, m_health, m_bonusProgress);

	if (m_paused)
	{
		SDL_SetRelativeMouseMode(SDL_FALSE);
		SDL_ShowCursor(0);
	}
	else
	{
		SDL_SetRelativeMouseMode(SDL_TRUE);

		if (m_mouse.isEnabled())
		{
			m_mouse.Update(m_paddles[0].get(), m_paddles[1].get(), m_paddles[2].get(), m_paddles[3].get());
		}
		else if (m_keyBoard.isEnabled())
		{
			m_keyBoard.Update(m_deltaTime, 75, m_screenWidth, m_screenHeight, *m_paddles[0], *m_paddles[1], *m_paddles[2], *m_paddles[3]);
		}

		m_HUD.Update(m_renderer, m_dungeonLevels, *m_ball, m_lives, m_health, m_bonusProgress);

		if (!m_paused)
		{
			m_ball->Update(m_deltaTime);
		}

		checkforGameOver();
		checkCollision();
		checkforBonus();
	}
}

void Gunngine::resetGame()
{
	m_health = 550;
	m_bonusProgress = 0;
	m_ball->resetBall(m_screenWidth, m_screenHeight, m_difficulty);

	if (!checkforWin())
	{
		if (m_lives > 0)
		{
			m_dungeonLevels.setLevel(m_dungeonLevels.getLevel());
		}
		else
		{
			if (!m_cached && m_dungeonLevels.getLevel() != 0)
			{
				m_dungeonLevels.Unload(m_dungeonLevels.getLevel());
				m_dungeonLevels.setLevel(0);
				m_dungeonLevels.Load(m_renderer);
			}
			else
			{
				m_dungeonLevels.setLevel(0);
			}

			m_lives = 3;
		}
	}

	m_paddles[0]->resetPaddles((m_screenWidth / 2) - (175 / 2), 30, (m_screenWidth / 6.5), 15);
	m_paddles[1]->resetPaddles(m_screenWidth - 45, (m_screenHeight / 2) - (175 / 2), 15, (m_screenHeight / 4));
	m_paddles[2]->resetPaddles((m_screenWidth / 2) - (175 / 2), m_screenHeight - 45, (m_screenWidth / 6.5), 15);
	m_paddles[3]->resetPaddles(30, (m_screenHeight / 2) - (175 / 2), 15, (m_screenHeight / 4));

	m_HUD.m_ScoreBoard->setScore(0);

	m_bgMusic.Play();

	m_newGame = true;
	m_paused = true;
	m_levelWon = false;
}

void Gunngine::cleanUp()
{
	printf("<-----CLEAN UP STARTED----->\n");

	SDL_DestroyRenderer(m_renderer);
	m_renderer = nullptr;

	SDL_DestroyWindow(m_window);
	m_window = nullptr;

	TTF_Quit();
	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}