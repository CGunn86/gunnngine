#pragma once

#include <SDL.h>
#include <cstdio>
#include <string>
#include <memory>
#include <array>
#include <cassert>
#include <random>

#include "Audio/Sound.h"
#include "Audio/Music.h"
#include "Entity/Ball.h"
#include "Entity/Item.h"
#include "Entity/Paddle.h"
#include "Graphics/Texture.h"
#include "UI/HUD.h"
#include "UI/Text.h"
#include "UI/Menu.h"
#include "Time/Timer.h"
#include "IO/Mouse.h"
#include "IO/Keyboard.h"
#include "Level/LevelSet.h"
#include "Physics/AABBCollision.h"

constexpr unsigned int NUM_PADDLES = 4;
constexpr unsigned int NUM_ITEMS = 5;
constexpr unsigned int NUM_LEVEL_SETS = 2;

class Gunngine
{
public:
	Gunngine()=delete;
	Gunngine(const std::string_view t, unsigned int w, unsigned int h);

	~Gunngine();

	bool init();
	void processInput();
	void Draw() const;
	void Update();

	bool isRunning() const { return m_running; }

private:
	//Gunngine(const Gunngine& obj)=delete;
	//Gunngine& operator= (const Gunngine&)=delete;

	void GameRunning(bool running) { m_running = running; }

	void updateDelta();
	void setupGameObjects();
	inline void setCache(bool v) { m_cached = v; }

	inline void beginRender() const { SDL_RenderClear(m_renderer); };
	inline void endRender() const { SDL_RenderPresent(m_renderer); };

	void drawLevel() const;
	void drawPaddles() const;
	void drawBall() const;
	void drawText() const;
	void drawCursor() const;
	void drawItems() const;

	void checkCollision();
	void checkforBonus();
	void checkforGameOver();
	bool checkforWin();

	void spawnItem();

	void resetGame();

	Paddle* getPaddle();

	void cleanUp();

	SDL_Window* m_window;
	SDL_Renderer* m_renderer;
	std::string m_title;
	unsigned int m_screenWidth;
	unsigned int m_screenHeight;
	unsigned int m_difficulty;
	int m_health;
	int m_bonusProgress;
	unsigned int m_lives;
	const char* m_convertedLives;
	std::string m_LivesString;

	Keyboard m_keyBoard;
	Mouse m_mouse;
	int m_mouseX;
	int m_mouseY;

	HUD m_HUD;

	LevelSet m_dungeonLevels;

	std::array<std::unique_ptr<Paddle>, NUM_PADDLES> m_paddles;
	std::array<std::unique_ptr<Item>, NUM_ITEMS> m_items;
	unsigned int m_itemNum;

	std::unique_ptr<Ball> m_ball;

	AABBCollision m_collider;

	Music m_bgMusic;
	Sound m_hitSound;
	Sound m_hitBadSound;
	Sound m_pauseSound;
	Sound m_gameOverSound;
	Sound m_heal;

	std::unique_ptr<Text> m_gameTitle;
	std::unique_ptr<Text> m_gameTitleStart;
	std::unique_ptr<Text> m_pausedText;
	std::unique_ptr<Text> m_pressSpaceText;
	std::unique_ptr<Text> m_gameOverText;
	std::unique_ptr<Text> m_gameOverNewGameText;
	std::unique_ptr<Text> m_levelPassed;
	std::unique_ptr<Text> m_levelPassedContinue;

	//Text m_gameTitle;
	//Text m_gameTitleStart;
	//Text m_pausedText;
	//Text m_pressSpaceText;
	//Text m_gameOverText;
	//Text m_gameOverNewGameText;
	//Text m_levelPassed;
	//Text m_levelPassedContinue;

	Texture m_dead;
	Texture m_cursor;

	timer_pool m_timerPool;

	double m_deltaTime;
	uint64_t m_currentTime;
	uint64_t m_lastTime;

	bool m_running;
	bool m_paused;
	bool m_gameOver;
	bool m_levelWon;
	bool m_newGame;
	bool m_cached;
};