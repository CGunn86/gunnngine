#include "LevelSet.h"

LevelSet::LevelSet(const char* filePath, unsigned int w, unsigned int h) :
m_levelTextures({}),
m_levelScore({0}), m_levelNum(0),
m_cached(false)
{
	assert(typeid(filePath) == typeid(const char*) && filePath > 0);
	assert(typeid(w) == typeid(unsigned int) && w > 0);
	assert(typeid(h) == typeid(unsigned int) && h > 0);

	for (size_t i = 0; i < m_levelTextures.size(); ++i)
	{
		std::ostringstream out;
		out << filePath << "level" << i << ".png";
		std::string filename = out.str();
		m_levelTextures[i] = Texture(filename);
		m_levelTextures[i].setRect(0, 0, w, h);
	}
}

LevelSet::~LevelSet()
{
	UnloadAll();
}

void LevelSet::Load(SDL_Renderer* passedRen)
{
	m_levelTextures[m_levelNum].Load(passedRen);
}

void LevelSet::LoadAll(SDL_Renderer* passedRen)
{
	for (size_t i = 0; i < m_levelTextures.size(); ++i)
	{
		m_levelTextures[i].Load(passedRen);
	}
}

void LevelSet::Unload(int x)
{
	m_levelTextures[x].Unload();
}

void LevelSet::UnloadAll()
{
	for (size_t i = 0; i < m_levelTextures.size(); ++i)
	{
		if (m_levelTextures[i].getTexture() != nullptr)
		{
			m_levelTextures[i].Unload();
		}
	}
}

void LevelSet::setScores(std::array<int, LEVEL_COUNT> s)
{
	for (size_t i = 0; i < s.size(); ++i)
	{
		m_levelScore[i] = s[i];
	}
}

void LevelSet::Draw(SDL_Renderer* passedRen) const
{
	SDL_Rect rect = m_levelTextures[m_levelNum].getRect();
	SDL_RenderCopy(passedRen, m_levelTextures[m_levelNum].getTexture(), nullptr, &rect);
}

void LevelSet::setLevel(unsigned int x)
{
	m_levelNum = x;
}

void LevelSet::nextLevel(SDL_Renderer* passedRen, bool v)
{
	if (m_levelNum < (m_levelTextures.size() - 1) && !v)
	{
		Unload(m_levelNum);
		++m_levelNum;
		Load(passedRen);
	}
	else if (m_levelNum < (m_levelTextures.size() - 1))
	{
		++m_levelNum;
	}
	else
	{
		if (!v)
		{
			Unload(m_levelNum);
			m_levelNum = 0;
			Load(passedRen);
		}
		else
		{
			m_levelNum = 0;
		}
	}
}

void LevelSet::prevLevel(SDL_Renderer* passedRen)
{
	if (m_levelNum >= 1)
	{
		Unload(m_levelNum);
		--m_levelNum;
		Load(passedRen);
	}
}