#include "Timer.h"

static uint32_t sdl_timer_callback(uint32_t interval, void* param)
{
	SDL_Event event;
	SDL_UserEvent usr_event;
	usr_event.type = SDL_USEREVENT;
	usr_event.code = 0;
	usr_event.data1 = nullptr;
	usr_event.data2 = nullptr;

	event.type = SDL_USEREVENT;
	event.user = usr_event;
	SDL_PushEvent(&event);

	return interval;
}

timer_pool::~timer_pool()
{
	for (auto timer : _timers)
		delete timer;
}

void timer_pool::Update()
{
	for (auto timer : _to_remove)
	{
		remove_timer(timer);
		_to_remove.clear();
	}

	const auto now = SDL_GetTicks();
	for (auto timer : _timers)
	{
		if (!timer->enabled)
			continue;

		if (timer->expiry_time < now)
		{
			if (!timer->callback(this, timer))
				timer->enabled = false;
			else
			{
				timer->expiry_time = now + timer->duration;
			}
		}
	}
}

timer_t* timer_pool::start_time(uint32_t duration, const timer_expired_callable& callable)
{
	auto timer = new timer_t;
	timer->id = s_id++;
	timer->enabled = true;
	timer->last_time = SDL_GetTicks();
	timer->expiry_time = timer->last_time + timer->duration;
	timer->callback = callable;
	_timers.emplace_back(timer);
	_id_to_timer.insert(std::make_pair(timer->id, timer));

	return(nullptr);
}

void timer_pool::mark_for_remove(timer_t* timer)
{
	_to_remove.emplace_back(timer);
}

bool timer_pool::remove_timer(timer_t* timer)
{
	auto it = _timers.erase(std::find_if(
		_timers.begin(),
		_timers.end(),
		[timer](timer_t* each) { return each == timer; }));

	_id_to_timer.erase(timer->id);
	delete timer;
	return it != _timers.end();
}

timer_t* timer_pool::find_by_id(uint16_t id)
{
	const auto it = _id_to_timer.find(id);
	if (it == _id_to_timer.end())
	{
		return(nullptr);
	}
	
	return it->second;
}