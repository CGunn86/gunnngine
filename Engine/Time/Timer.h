#pragma once
#include <SDL.h>
#include <cstdio>
#include <vector>
#include <functional>
#include <algorithm>
#include <unordered_map>

class timer_pool;
struct timer_t;

using timer_expired_callable = std::function<bool(timer_pool*, timer_t*)>;

struct timer_t
{
	uint16_t id;
	bool enabled;
	uint32_t last_time;
	uint32_t expiry_time;
	uint32_t duration;
	uint32_t data1;
	uint32_t data2;

	timer_expired_callable callback {};
};

using timer_map_t = std::unordered_map<uint16_t, timer_t*>;
using timer_list_t = std::vector<timer_t*>;

class timer_pool
{
public:
	timer_pool()=default;
	~timer_pool();

	void Update();

	timer_t* start_time(uint32_t duration, const timer_expired_callable& callable);
	void mark_for_remove(timer_t* timer);
	bool remove_timer(timer_t* timer);
	timer_t* find_by_id(uint16_t id);
	const timer_list_t& timers() const { return _timers; }

private:
	static inline uint16_t s_id = 1;

	timer_list_t _timers{};
	timer_list_t _to_remove{};
	timer_map_t _id_to_timer{};
};