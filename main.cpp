#include <cstdio>
#include <memory>
#include "Engine/Gunngine.h"

//TODO: (GLOBAL) Create a buff and debuff item system (Calvin)
//TODO: (GLOBAL) Do I want a singular base audio class to inherit or keep separate effects and music classes? (Calvin)
//TODO: (GLOBAL) Revamp the collision class to handle ALL types of collisions. Paddle->Ball, and Ball->Item (Calvin)

int main(int argc, char* argv[])
{
	(void*)argc;
	(void*)argv;

	std::unique_ptr<Gunngine> game = std::make_unique<Gunngine>("Keep it Alive - A Pong Game", 1366, 768);

	if (game->init())
	{
		while (game->isRunning())
		{
			game->processInput();
			game->Update();
			game->Draw();
		}

		printf("<-----GAME EXITING--------->\n");
	}
	else
	{
		printf("Error starting game. Restart the app.");
		return(-1);
	}

	return(0);
}